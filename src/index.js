/*
1. Todos los derechos de propiedad intelectual e industrial sobre el código
fuente y el código objeto de la tecnología que se pone a disposición,
pertenecen a Instituto Tecnológico de Informática (ITI), incluido los
materiales preparatorios, manuales, diagramas, contenidos, diseños, interfaces
y obras sometidas a derechos de propiedad intelectual relacionados con los
programas informáticos, sus modificaciones y futuras versiones.
 
2. El Usuario se compromete a no desvelar, comunicar o, bajo cualquier forma,
transmitir o ceder el citado código sin autorización previa, expresa y por
escrita  de ITI.
*/
var authenticate = require("./authenticate");
var authorize = require("./authorize");

module.exports = authenticate;

module.exports.router = require('./router');
module.exports.configureExpress = authenticate.configureExpress;
module.exports.configureIO = authenticate.configureIO;

module.exports.authorize = authorize;
module.exports.tkm = require('./token-manager');