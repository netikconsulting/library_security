/*
1. Todos los derechos de propiedad intelectual e industrial sobre el código
fuente y el código objeto de la tecnología que se pone a disposición,
pertenecen a Instituto Tecnológico de Informática (ITI), incluido los
materiales preparatorios, manuales, diagramas, contenidos, diseños, interfaces
y obras sometidas a derechos de propiedad intelectual relacionados con los
programas informáticos, sus modificaciones y futuras versiones.
 
2. El Usuario se compromete a no desvelar, comunicar o, bajo cualquier forma,
transmitir o ceder el citado código sin autorización previa, expresa y por
escrita  de ITI.
*/
var jwt = require('jsonwebtoken');
var utils = require('cysmeter-utils');

var secretToken = null;
var tokenOnce = [];
var api={};

var purposes = {};
purposes.restAPI = "restAPI";
purposes.resetPassword = "resetPassword";

var timeouts={};

var userTokenTimeout = null;
var resetPasswordTokenTimeout = null;
var maxTokenTimeout = null;

api.purposes = purposes;

api.userToken = function (userId, userType){
  console.log("Creando userToken con timeout", userTokenTimeout);
  var result = api.sign({user: userId, type: userType}, userTokenTimeout);
  console.log("U Token:", result);
  return result;
};

api.changePasswordToken = function (userId){
  console.log("Creando resetPasswordToken con timeout", resetPasswordTokenTimeout);
  var result = api.sign({user: userId,
      purpose: purposes.resetPassword,
      once:true,
      rnd:Math.floor(Math.random()*10000),
      type: 'frontend'
    }, resetPasswordTokenTimeout);
  console.log("RP Token:", result);
  return result;
};

api.sign = function (data, expiration){
  return jwt.sign(data, secretToken , {expiresInMinutes: expiration});
};

api.verify =  function(token, cb){
  jwt.verify(token, secretToken, function (err, decodedToken){
    if (err){return cb(err);}
    var found = tokenOnce.indexOf(token);
    if (found != -1){
      return cb(utils.formatErrorMessage("auth:tokenOnce"));
    }
    if (decodedToken.once){
      tokenOnce.push(token);
      setTimeout(function(){
        var i = tokenOnce.indexOf(token);
        if(i != -1) {tokenOnce.splice(i, 1);}
      }, maxTokenTimeout);
    }
    cb(err,decodedToken);
  });
};

api.comparePurpose = function(decodedToken, purpose){
  if (!decodedToken || !purpose){return false;}
  decodedPurpose = decodedToken.purpose;
  if (!decodedPurpose){ decodedPurpose = purposes.restAPI;}
  return decodedPurpose+"" == purpose+"";
};

api.timeout = function(purpose){
  return timeouts[purpose];
};

api.init = function(config){
  secretToken = config.secretToken;
  userTokenTimeout = config.userTokenTimeout;
  resetPasswordTokenTimeout = config.resetPasswordTokenTimeout;
  timeouts[purposes.restAPI] = userTokenTimeout;
  timeouts[purposes.resetPassword] = resetPasswordTokenTimeout;
  maxTokenTimeout = Math.max.apply(Math, [userTokenTimeout, resetPasswordTokenTimeout])*60000;
};

module.exports = api;

