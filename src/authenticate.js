/*
1. Todos los derechos de propiedad intelectual e industrial sobre el código
fuente y el código objeto de la tecnología que se pone a disposición,
pertenecen a Instituto Tecnológico de Informática (ITI), incluido los
materiales preparatorios, manuales, diagramas, contenidos, diseños, interfaces
y obras sometidas a derechos de propiedad intelectual relacionados con los
programas informáticos, sus modificaciones y futuras versiones.
 
2. El Usuario se compromete a no desvelar, comunicar o, bajo cualquier forma,
transmitir o ceder el citado código sin autorización previa, expresa y por
escrita  de ITI.
*/
var passport = require('passport');
var utils = require('cysmeter-utils');
var util = require('util');
var handleKeyErr = utils.handleKeyErr;
var auth = null;
var BasicStrategy = require('passport-http').BasicStrategy;
var models = null;
var tkm = null;

var sec = function(auth_cfg){

  return function(req, res, next) {
    var resf={};
    resf.setHeader=function (key,value){
      res.setHeader(key,value);
    };

    // Autorización
    var nextf=function(){
      auth.authorize(req, auth_cfg, function(err,authorized){
        if (err && err.subcode){return utils.handleErr(err,res);}
        if (err){return res.status(401).send('Unauthorized: '+err);}
        else {
          if (authorized){return next();}
          return handleKeyErr("auth:entityUnknown", res);
        }
      });
    };

    // Autenticación con token
    var handleToken = function(){
      jwtAuthenticate(req, function(err){
        if (err && err.subcode){return utils.handleErr(err,res);}
        if (err){return res.status(401).send('Unauthorized: '+err);}
        if (req.user){ return nextf();}
        res.status(401).send('Unauthorized');
      });
    };

    // Autenticación con certificado
    var certAuth = function(){
      certAuthenticate(req, function(err){
        if (req.user){ return nextf();}
        res.status(401).send('Unauthorized');
      });
    };
    console.log("req.client.authorized", req.client.authorized);
    if (req.client.authorized){
      resf.end = certAuth;
    } else {
      resf.end = handleToken;
    }
    passport.authenticate('basic', { session: false })(req,resf,nextf);
  };
};

var getHeaderToken = function(headers) {
  if (headers && headers.authorization) {
    var authorization = headers.authorization;
    var part = authorization.split(' ');

    if (part.length == 2) {
      var token_type = part[0];
      var token = part[1];
      if (token_type.toLowerCase() != 'bearer') { return null;}
      return part[1];
    }
    else {
      return null;
    }
  }
  else {
    return null;
  }
};

var getQueryToken = function(data){
  if (data){return data.access_token;}
  return null;
};

var getToken = function(req){
  var token = getHeaderToken(req.headers);
  if (token){req.tokenPlace='headers'; return token;}
  token = getQueryToken(req.body);
  if (token){req.tokenPlace='body'; return token;}
  token = getQueryToken(req.query);
  if (token){req.tokenPlace='query'; return token;}
  return null;
};

var jwtAuthenticate = function(req, cb){
  var model = models.userModel;
  var token = getToken(req);
  if (!token){return cb(null,false);}
  tokenAuthenticate(token,function (err,payload){
    checkTokenPayload(err, payload, req, cb);
  });
};

var checkTokenPayload = function(err, payload, req, cb){
  if (err){
    if (err.name == "TokenExpiredError"){return cb(utils.formatErrorMessage("auth:expiredToken"));}
    if (err.code == "MISSING_HEADER"){return cb(utils.formatErrorMessage("auth:badToken"));}
    return cb(err);
  }
  if (!payload || !payload.user || !payload.iat || !payload.exp){
    return cb(utils.formatErrorMessage("auth:badToken"));
  }
  var model = payload.type == 'frontend' ? models.userModel : models.gatewayModel;
  
  model.findById(payload.user, function (err,user){
    if (!user){console.log("Usuario desconocido",payload.user);return cb("auth:entityUnknown");}
    console.log("El usuario del token es",(payload.type == 'frontend')?user.username:user.name);
    var prepareFunc = null;
    if (payload.type == 'frontend'){
      if (!userValid(user)){return cb(utils.formatErrorMessage("auth:staleToken"));}
      if (user.lastPasswordChange && user.lastPasswordChange>(new Date(payload.iat*1000))){
        console.log("Token obsoleto");
        return cb(utils.formatErrorMessage("auth:staleToken"));
      }
      prepareFunc = prepareRequestUser;
    }
    if (payload.type == 'gateway'){
      if (!gwValid(user)){return cb(utils.formatErrorMessage("auth:staleToken"));}
      prepareFunc = prepareRequestGW;
    }
    prepareFunc(user, payload.user, function (err, reqUser){
      if (err){return cb(err);}
      if (req){
        req.user=reqUser;
        req.token = payload;
      }
      return cb(null,payload);
    });
  });
};

var userValid = function (user){
  return user && user.enabled;
};

var gwValid = function (gw){
  return gw && gw.enabled;
};

var isGWCertificate = function(cert){
  return true;
};

var tokenAuthenticate = function(token, cb){
  if (!token) {return cb();}
  console.log("Examinando token");
  tkm.verify(token, function(err, decoded) {
    console.log("tkm.verify(token, function(err, decoded)",err,decoded);
    if (err){return cb(err);}
    if (!decoded){return cb(utils.formatErrorMessage("auth:badToken"));}
    if (decoded){
      console.log("Token valido con usuario",decoded.user);
      cb(null, decoded);
    }
  });
};

var certAuthenticate = function(req, cb){
  if(req.client.authorized){
    var subject = req.connection.getPeerCertificate().subject;
    console.log('Autenticando gateway', subject.CN);
    if (isGWCertificate(subject)){
      var cn = subject.CN;
      models.gatewayModel.findByCert(cn, function(err,gw){
        if (gwValid(gw)){
          prepareRequestGW(gw, cn, function (err, reqGw){
            if (err) {return cb(err);}
            req.user = reqGw;
            return cb();
          });
        } else {
          console.log('Identificador de gateway no válido. No hay información sobre él en el sistema.', subject.CN);
          return cb();
        }
      });
    }
  }
};

var basicStrategy = new BasicStrategy({},
  function(username, password, done) {
    var model = models.userModel;
    model.findByName(username , function (err, user) {
      if (err) { return done(err); }
      if (!userValid(user)) {
        return done(null, false, { message: 'Incorrect username or password' });
      }
      user.comparePassword(password,function(err,isMatch){
        if (err){return done(err, false, { message: err });}
        if (!isMatch){console.log("Password incorrecto");return done(null, false, { message: 'Incorrect username or password' });}
        console.log("Verificado password");
        prepareRequestUser(user, username, done);
      });
    });
  }
);

var prepareRequestUser = function(user, username, cb){
  var juser = user.toObject();
  delete juser.password;
  juser.id = juser._id;
  updateRequestCount(user.company);
  models.companyModel.findById(user.company, function (err,company){
    if (err) return cb(err);
    if (!company.enabled){return cb(null, false, { message: 'Disabled company' });}
    juser.company = company;
    if (company.masterCompany){
      juser.company._id = company.masterCompany;
    }
    cb(null, juser);
  });
};

var prepareRequestGW = function(gw, cn, cb){
  models.companyModel.findById(gw.company, function (err,company){
    if (!company.enabled){return cb("Disabled company");}
    var juser = gw.toObject();
    juser.id = juser._id;
    juser.roles = ['GATEWAY'];
    juser.username = cn;
    juser.gateway = true;
    cb(null, juser);
  });
};

var configureExpress = function(app){
  app.use(passport.initialize());
  app.use('/auth', require('./router'));
};

var configureIO = function(io){
  io.use(function (socket, next){

    var reject = function(){
      return next(new Error('Not Authorized'));
    } ;

    socket.on('error', function(err){
      console.log('error on ws', err);
    });

    //console.log("data:",data);
    //console.log("Handshake:",socket.handshake.query.token);
    var token = socket.handshake.query.token;
    console.log("Autenticando websocket handshake con token",token);
    if (!token){return reject();}
    tokenAuthenticate(token,function (err,payload){
      checkTokenPayload(err, payload, socket, next);
    });
  });
};

function updateRequestCount(company){
  models.monthRequestModel.inc(company,'front');
}

passport.use(basicStrategy);

module.exports = sec;

module.exports.init = function (pModels, pAuthorize, pTkm){
  models = pModels;
  auth = pAuthorize;
  tkm = pTkm;
};

module.exports.configureExpress = configureExpress;
module.exports.configureIO = configureIO;
module.exports.tokenAuthenticate = tokenAuthenticate;
module.exports.checkTokenPayload = checkTokenPayload;

