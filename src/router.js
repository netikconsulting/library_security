/*
1. Todos los derechos de propiedad intelectual e industrial sobre el código
fuente y el código objeto de la tecnología que se pone a disposición,
pertenecen a Instituto Tecnológico de Informática (ITI), incluido los
materiales preparatorios, manuales, diagramas, contenidos, diseños, interfaces
y obras sometidas a derechos de propiedad intelectual relacionados con los
programas informáticos, sus modificaciones y futuras versiones.
 
2. El Usuario se compromete a no desvelar, comunicar o, bajo cualquier forma,
transmitir o ceder el citado código sin autorización previa, expresa y por
escrita  de ITI.
*/
var express = require('express');
var router = express.Router();
var utils = require('cysmeter-utils');
var formatMessage = utils.formatErrorMessage;
var handleKeyErr = utils.handleKeyErr;
var handleErr = utils.handleErr;

router.init = function(serviceP, security, tkm, mailService, config){
  var userService = serviceP;
  var sec = security;

var login = function(req, res) {
  var userType = req.user.gateway ? "gateway" : "frontend";
  var token = tkm.userToken(req.user.id, userType);
  var result = function(err){
    if (err){console.log("Error:",err);}
    var r = {access_token:token,token_type:'bearer',
                  expires_in : tkm.timeout(tkm.purposes.restAPI)};
    return res.json(r);
  };
  if (!req.token && !req.user.gateway){ return userService.touchLogin(req.user.id, result);}
  else {result();}
};

router.get('/login',sec({role:'AUTHENTICATED'}), login);
router.post('/login',sec({role:'AUTHENTICATED'}), login);


router.post('/changePassword',sec({purpose:tkm.purposes.resetPassword}),function(req, res) {
  var password = req.body.password || req.query.password;
  if (!password){return handleKeyErr('requiredParameter',['password'], res);}
  userService.changePassword(req.user.id, password, function (err){
    if (err) {return res.status(500).send(err);}
    return res.json({msg:"OK"});
  });
});

router.get('/resetPassword',function(req, res) {
  var username = req.query.username;
  var url = req.query.url||"http://localhost:3000";
  if (!username){return handleKeyErr('requiredParameter',['username'], res);}
  if (!url){return handleKeyErr('requiredParameter',['url'], res);}
  userService.findByName(username, function(err, user){
    if (err){return res.status(500).send(err);}
    if (!user){
      //return res.status(401).send('User unknown');
      // We don't have clues about our users
      console.log("Solicitud de reset password a usuario inexistente",username);
      return res.json({msg:"OK"});
    }
    var timeout = tkm.timeout(tkm.purposes.resetPassword);
    var token = tkm.changePasswordToken(user.getId());
  
    var templateValues = {
       fullname: user.fullName,
       username: username,
       url: url,
       token: token,
       timeout: timeout
    };
    var message = {
     to: user.fullName+"<"+username+">",
    };
    mailService.sendTemplateMail(message, 'resetPassword', templateValues, router.handleMailCB(res));
  });
});

router.handleMailCB = function(res){
  return function(err, data){
    if (err){
      console.log("Error en el envío de correo",err);
      return res.status(500).send(err);
    }
    return res.json({msg:"OK"});
  };
};

};

module.exports = router;