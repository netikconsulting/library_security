/*
1. Todos los derechos de propiedad intelectual e industrial sobre el código
fuente y el código objeto de la tecnología que se pone a disposición,
pertenecen a Instituto Tecnológico de Informática (ITI), incluido los
materiales preparatorios, manuales, diagramas, contenidos, diseños, interfaces
y obras sometidas a derechos de propiedad intelectual relacionados con los
programas informáticos, sus modificaciones y futuras versiones.
 
2. El Usuario se compromete a no desvelar, comunicar o, bajo cualquier forma,
transmitir o ceder el citado código sin autorización previa, expresa y por
escrita  de ITI.
*/
var models = null;
var tkm = null;
var async = require('async');
var utils = require('cysmeter-utils');

var authorize = function(req, auth, done){
  console.log("Entro en authorize");
  var user = req.user;
  if (!user){return done(null,false);}
  if (req.token){
    console.log("Examinando purpose de token");
    // por defecto, un metodo limita su acceso a tokens de proposito restAPI
    var purpose = tkm.purposes.restAPI;
    if (auth && auth.purpose){ purpose = auth.purpose;}
    if (!tkm.comparePurpose(req.token, purpose)){
      return done(utils.formatErrorMessage("auth:invalidTokenPurpose"), false);
    }
    console.log("Examinando place de token");
    // por defecto, un metodo limita su acceso a tokens en headers
    var place = ['headers'];
    if (auth && auth.tokenPlace){ place = auth.tokenPlace;}
    if (place.indexOf(req.tokenPlace)<0){
      console.log("Mala localización de token", place, req.tokenPlace);
      return done(utils.formatErrorMessage("auth:invalidTokenLocation"), false);
    }
  }

  var roles = user.roles;
  if (roles.indexOf("ADMIN")>=0) { return done(null,true);}

  if (!auth){auth = {role:'USER'};}

  async.waterfall([
    function checkRoles(cb){
      if (auth.role){
        // sec({role:'ADMIN'})
        if (hasRole(user,auth.role)){return cb(null,true);}
        else {return cb(null,false);}
      }
      cb(null,true);
    },
    function checkRoleChange(prev, cb){
      if (!prev) {return cb(null,prev);}
      if (auth.roleChange){
        var toValid = eval(auth.roleChange);
        if (!toValid){return cb(null,true);}
        if (!utils.isArray(toValid)){toValid = [toValid];}
        for (var i = toValid.length - 1; i >= 0; i--) {
          if (!canAssignRole(user,toValid[i])){
            console.log("User can't assign that role",user.username,toValid[i]);
            return cb(null,false);
          }
        }
      }
      cb(null,true);
    },
    function checkPermissions(prev,cb){
      if (!prev) {return cb(null,prev);}
      var toCheck = [];
      for (var i in auth){
        if (i == 'role' || i == 'purpose'|| i == 'roleChange' || i == 'tokenPlace'){continue;}
        toCheck.push({key:i, value:auth[i]});
      }
      var permissionsError = null;
      async.every(toCheck, function (item, permCB){
        permissions[item.key](item.key,eval(item.value),user, function (err,permResult){
          if (err){
            if (!permissionsError) {permissionsError = err;}
            return permCB(false,err);
          }
          console.log("El permiso "+item.key+" retornó "+permResult);
          permCB(permResult);
        });
      },
      function (result){cb(permissionsError,result);}
      );
    }
  ], function(err, result){
    if (err){return done(err);}
    done(null,result);
  });
};

var companyAdminPermission = function(key, value, user, cb){
    if (user.roles.indexOf("COMPANY_ADMIN")>=0){
      if (value == user.company._id){ return cb(null, true);}
      else {return cb(utils.formatErrorMessage("auth:entityUnknown",[value]), false);}
    }
    return cb(null, false);
};

// Acceso a Dao de xxxx, ver roles del usuario
// comprobar compañía de xxxx y del usuario autenticado es la misma
var genericAdminPermission = function(key, value, user, cb){
  console.log("Entro en genericAdminPermission");
  if (!value){return cb(utils.formatErrorMessage("auth:entityUnknown"), false);}
  if (key.toLowerCase().indexOf("admin")>=0 && !hasRole(user,"COMPANY_ADMIN")) {return cb(null, false);}
  var model = permissionModels[key];
  if (!model){ return cb('Permission unknown '+key);}
  if (!utils.isArray(value)){
    var maybe =value.split(" ");
    if (maybe.length>1){value = maybe;}
    else {value=[value];}
  }
  console.log("Values",value);
  async.every(value,function checkIndividualPermission(iEntity, icb){
    model.findById(iEntity, function (err, dbEntity){
      if (err) {
        console.log("Error on genericAdminPermission", err);
        return icb(false);
      }
      if (!dbEntity) {return icb(false);}
      return icb(dbEntity.company.equals(user.company._id));
    });
  }, function(result){
    if (!result) {return cb(utils.formatErrorMessage("auth:entityUnknown"), false);}
    cb(null, true);
  });
};

var deviceCheckRepeaterPermission = function(key, value, user, cb){
  models.deviceModel.findById(value, function(err,device){
    if (err){ return cb(err);}
    if (device.gateway.equals(user._id)){return cb(null,true);}
    return cb(null,false);
  });
};

var deviceSetRepeaterPermission = deviceCheckRepeaterPermission;

var deviceUpdateSubscribePermission = deviceCheckRepeaterPermission;

var hasRole = function(user, role){
  if (role == 'AUTHENTICATED'){ return user;}
  var roles = user.roles;
  if (!roles){return false;}
  if (roles.indexOf(role)>=0){return true;}
  if (role == 'USER'){ return hasRole(user,'ADMIN') || hasRole(user,'COMPANY_ADMIN');}
  if (role == 'COMPANY_ADMIN'){ return hasRole(user,'ADMIN');}
  return false;
};

var canAssignRole = function(user,newRole){
  return hasRole(user,newRole);
};

var permissions = {
  companyAdminPermission      : companyAdminPermission,
  userAdminPermission         : genericAdminPermission,
  locationAdminPermission     : genericAdminPermission,
  locationQueryPermission     : genericAdminPermission,
  gatewayAdminPermission      : genericAdminPermission,
  gatewayQueryPermission      : genericAdminPermission,
  deviceAdminPermission       : genericAdminPermission,
  deviceQueryPermission       : genericAdminPermission,
  alarmAdminPermission        : genericAdminPermission,
  alarmQueryPermission        : genericAdminPermission,
  modelAdminPermission        : genericAdminPermission,
  modelQueryPermission        : genericAdminPermission,
  deviceSetRepeaterPermission : deviceSetRepeaterPermission,
  deviceCheckRepeaterPermission: deviceCheckRepeaterPermission,
  deviceUpdateSubscribePermission: deviceUpdateSubscribePermission
};

var permissionModels = null; /* Values are set in the init */

module.exports.init = function(pModels, pTkm){
  models = pModels;
  permissionModels = {
    userAdminPermission      : models.userModel,
    deviceQueryPermission    : models.deviceModel,
    deviceAdminPermission    : models.deviceModel,
    gatewayQueryPermission   : models.gatewayModel,
    gatewayAdminPermission   : models.gatewayModel,
    locationQueryPermission   : models.locationModel,
    locationAdminPermission   : models.locationModel,
    alarmAdminPermission      : models.alarmModel,
    alarmQueryPermission      : models.alarmModel,
    modelAdminPermission      : models.modelModel,
    modelQueryPermission      : models.modelModel
  };
  tkm = pTkm;
};

module.exports.authorize = authorize;